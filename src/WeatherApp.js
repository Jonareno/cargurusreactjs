import React, { useContext } from 'react';
import './App.css';
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import WeatherPanelList from './WeatherPanelList';
import ZipCodeForm from './ZipCodeForm';
import { WeatherService } from './weather-service-api';
import { WeatherStateContext } from './weather-state';

/*
  Yes, I am importing ~4mb of libraries for this demo app. It's overkill but I did it deliberately
  to maximize appearance with minimal style work. The scope of this was to use the React framework :) 
 */
function WeatherApp() {
  const { zipWeatherDataMap, setZipWeatherDataMap } = useContext(WeatherStateContext); // Could be pulled out to it's own hook

  // This technically doesn't need to be here, and could live in the ZipCodeForm. Just showing we can pass functions to props.
  const handleFormSubmit = async (zipCode) => {
    if (zipCode) {
      const weatherData = await WeatherService.getWeatherByZipCode(zipCode);
      setZipWeatherDataMap({ ...zipWeatherDataMap, [zipCode]: weatherData });
    }
  };

  return (
    <div className="App">
      <ZipCodeForm handleFormSubmit={handleFormSubmit} />
      <WeatherPanelList />
    </div >
  );
}

export default WeatherApp;
